// PollumComEStrategy.js

const Utils = require('./Utils');
class PollumComEStrategy {
    constructor(parent,sendSerialMessage) {
        this.resetDecoder();
        this.parent = parent;
        this.sendSerialMessage = sendSerialMessage;
        this.response = { // response ist das Objekt, das alle dekodierten Informationen enthält und das ausgegeben werden soll.
            'dataLength':0,
            'address':0,
            'meterID':0,
            'manufactureID':0,
            'generation':0,
            'medium':0,
            'page':0
        }
        this.unkownVIBCounter = 1; // Falls das VIB unbekannt ist, wird ein generisches Attribut hochgezählt
        this.strategyState = PollumComEStrategy.states.wakeup;
        this.expactingAnotherResposne = true;
    }
    static DEBUG = true;
    static DECODE = false;
    static WAKEUP_HEX_STRING = '55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55 55';
    static READY_CHECK_HEX_STRING = '10 40 00 40 16';
    static REQUEST_SHORT_DATA_HEX_STRING = '10 5B 00 5B 16';
    static REQUEST_LONG_DATA_HEX_STRING = '10 7B 00 7B 16';
    static END_SYMBOL_HEX_STRING= '16';

    static states = {
        wakeup: 'wakeup',
        request_short: 'request_short',
        request_long: 'request_long',
        reset: 'reset'
    }


    interpret(buf) {
        switch (this.strategyState) {
            case PollumComEStrategy.states.wakeup:
                // e5 ist die Bestätigung, dass die Gegenstelle aktiv ist.
                if(buf.equals(Utils.convertHexStringToByteBuffer("e5"))) {
                    //Status auf Grün Setzen
                    this.parent.status({ fill: 'green', shape: 'dot', text: 'Gegenstelle hat geantwortet'})


/*                    if(PollumComEStrategy.DEBUG) {
                        console.log(this.msg.payload)
                        // Aktuell kann aus irgendeinem Grund nicht der Wert aus der GUI Übernommen werden. Eigentlich sollte "this.parent.guiConfig.serial_getShortData_msg" den wert aus der GUI enthalten
                        console.log("parent.guiConfig.serial_getShortData_msg:"+this.parent.guiConfig.serial_getShortData_msg);
                        console.log(this.parent)
                    }*/

                    //Daten holen
                    this.strategyState = PollumComEStrategy.states.request_short;
                    this.sendRequestMessage();
                    return null;


                } else {
                    //todo, was passiert wenn nicht e5 kommt , timeout
                    return null;
                }

            case PollumComEStrategy.states.request_short:
                return this.interpretRequestedData(buf);

            case PollumComEStrategy.states.request_long:
                return this.interpretRequestedData(buf);
        }
    }

    interpretRequestedData(buf) {
        this.byteBuffer = Buffer.concat([this.byteBuffer,buf],this.byteBuffer.length+buf.length) // Ein Buffer mit allen Hex-Werten.
        this.byteAsStringArr = Utils.convertByteBufferToHexArray(this.byteBuffer);

        //Prüfung, ob die Länge bereits eingelesen wurde (2. und 3. Byte). Falls Ja, wird diese geprüft und gespeichert
        if(this.byteAsStringArr.length > 2 && this.incomingMSGlenght === null) {
            if(this.byteAsStringArr[1]!==this.byteAsStringArr[2]){
                console.error("Längenbytes der Empfangenen Nachricht sind nicht gleich" + this.byteAsStringArr);
            }else {
                this.incomingMSGlenght = parseInt(this.byteAsStringArr[1],16);
                this.response.dataLength = this.byteAsStringArr[1];
                if(PollumComEStrategy.DEBUG){
                    console.log('Node: ' + this.parent.guiConfig.name + ' Bytebuffer: '+ this.byteBuffer);
                    console.log('Node: ' + this.parent.guiConfig.name + ' byteAsStringArr: '+ this.byteAsStringArr);
                    console.log('Node: ' + this.parent.guiConfig.name + ' Gewandelte Bytebuffer'+Utils.convertByteBufferToHexArray(this.byteBuffer));
                    console.log('Node: ' + this.parent.guiConfig.name + ' incomingMSGlength = '+this.incomingMSGlenght);
                }
            }
            return null;
        }

        // Es wird anhand der Länge des Arrays geprüft wie viel Bytes empfangen wurden. Dann wird geprüft, ob man am Ende der Nachricht ist, dann wird nach dem Endzeichen gesucht.
        if(this.byteAsStringArr.length >= (this.incomingMSGlenght + 5)) {// +5 Weil die ersten 4 Bytes und das Endzeichen nicht bei der Länge mitgezhählt werden
            if (this.byteAsStringArr[this.incomingMSGlenght + 5] === PollumComEStrategy.END_SYMBOL_HEX_STRING) {

                //Aktuell ist das Dekodieren von Nachrichten an dieser Stelle deaktiviert.
                // Es werden nur die Bytes gelesen und interpretiert die zum Erkennen der Länge/Ende notwendig sind
                // und um zu gucken, ob noch weitere Nachrichten angefragt werden müssen

                if(PollumComEStrategy.DECODE){
                    this.decodeMSG();
                }
                this.parent.status({fill: 'green', shape: 'dot', text: 'Übertragung Abgeschlossen'});
                const returnObj = this.response;
                const returnString = this.byteAsStringArr.join(" ");

                this.checkForMoreTelegrams(this.byteAsStringArr[this.incomingMSGlenght + 3]);

                return {value: returnString, obj: returnObj,nextMessage:this.strategyState };
                //return returnString;

                // Falls man weit über das Ende hinweg noch daten bekommt wird trotzdem versucht die Nachricht zu dekodieren
            } else if(this.byteAsStringArr.length >= (this.incomingMSGlenght + 56)) {
                try {
                    if(PollumComEStrategy.DEBUG){
                        console.log('Node: ' + this.parent.guiConfig.name + 'kein Endzeichen gelesen, es wird trotzdem versucht die Nachricht zu dekodieren')
                    }
                    //Aktuell ist das Dekodieren von Nachrichten an dieser Stelle deaktiviert.
                    // Es werden nur die Bytes gelesen und interpretiert die zum Erkennen der Länge/Ende notwendig sind
                    // und um zu gucken ob noch weitere Nachrichten angefragt werden müssen

                    //this.decodeMSG()
                    this.parent.status({fill: 'green', shape: 'dot', text: 'Übertragung Abgeschlossen'});
                    return this.byteAsStringArr.join(" ");
                    //return this.response;
                } catch (error) {
                    this.parent.status({fill: 'red', shape: 'dot', text: error.message});
                    console.error("Fehler beim Iterieren der Nachricht" + error.message);
                    return null;
                }
            }
        }
        return null;
    }
    checkForMoreTelegrams(byteAsString){
        if(PollumComEStrategy.DEBUG){
            console.log('Node: ' + this.parent.guiConfig.name + " byteAsString Kommen mehr Telegramme (1f?): "+byteAsString);
        }
        //wenn noch was kommt (1f)
        if(byteAsString === "1f"){
            this.switchRequestingState();
        } else {
            this.resetDecoder();
            this.strategyState = PollumComEStrategy.states.reset;
        }
    }
    switchRequestingState(){
        //wechseln vom short Request zum long Request
        if(this.strategyState === PollumComEStrategy.states.request_long){
            this.resetDecoder();
            this.strategyState = PollumComEStrategy.states.request_short;
        }else if(this.strategyState === PollumComEStrategy.states.request_short) {
            this.resetDecoder();
            this.strategyState = PollumComEStrategy.states.request_long;
        }
    }
    sendRequestMessage(){
        if(PollumComEStrategy.DEBUG){
            console.log('Node: ' + this.parent.guiConfig.name + " State in sendRequestMessage: "+this.strategyState);
        }
        if(this.strategyState === PollumComEStrategy.states.request_short){
            this.sendSerialMessage(Utils.convertHexStringToByteBuffer(PollumComEStrategy.REQUEST_SHORT_DATA_HEX_STRING), 'even');
        }else if(this.strategyState === PollumComEStrategy.states.request_long){
            this.sendSerialMessage(Utils.convertHexStringToByteBuffer(PollumComEStrategy.REQUEST_LONG_DATA_HEX_STRING), 'even');
        }
    }
/*
todo: sollte in eigenen Node ausgelagert werden.
 */
    decodeMSG() {
        if(PollumComEStrategy.DEBUG) {
            console.log(this.byteAsStringArr);
            console.log(this.byteBuffer);
        }
        // Überprüfen der Bytes an Stelle 2 und 3. Diese sind die Länge der Nachricht in Hex und müssen gleich sein
        if(this.byteBuffer[1]!==this.byteBuffer[2]){
            console.error("Längenbytes der Empfangenen Nachricht sind nicht gleich");
        }else {
            this.response.dataLength=this.byteAsStringArr[1];
        }
        //Adresse des Auszulesenden Energiezählers
        this.response.address = this.byteAsStringArr[5];

        //Zähler-ID in umgekehrter Reihenfolge 4 Bytes
        this.response.meterID = this.byteAsStringArr[10] + this.byteAsStringArr[9] + this.byteAsStringArr[8] + this.byteAsStringArr[7];

               //Hersteller 2 Bytes als Little Endian //todo "switchcase Für Hersteller"
        const hersteller = this.byteAsStringArr[12] + this.byteAsStringArr[11]; // https://www.m-bus.de/man.html Berechnung für MAN und Liste der Hersteller
        if(hersteller === "4cae") {
            this.response.manufactureID = "Sensus";
        } else {
            this.response.manufactureID = hersteller;
        }

        // Generation
        this.response.generation = this.byteAsStringArr[13];

        // Medium // todo Switch case für Medium
        if(this.byteAsStringArr[14] === "04") {
            this.response.medium = "Wärmerer Strang im Rücklauf"
        } else {
            this.response.medium = this.byteAsStringArr[14];
        }
        // Seite
        this.response.page = this.byteAsStringArr[15];

        if(PollumComEStrategy.DEBUG) {console.log(this.response);}
        // Die ersten 18 Byte sind der Statische 'header'. Ab der 19 Stelle wird über den Rest des Buffers iteriert
        let i = 19;
        do {
            //Das erste Byte für ein Attribut ist immer der DIB (Data Information Block).
            //Es können bis zu 10 Extension Blöcke folgen.
            const dib = this.getExtensionBlocks(this.byteAsStringArr, i);

            // Die dibMethod ist die passende Methode zum decodieren des Values, passend zum DIB (z.B. 0x0c = BCD8) Die Passenden Methoden sind in der Utils Klasse beschrieben.
            // Mit dem Bracket-Operator [] wird die passende Methode zum Key gefunden todo: Fehlermeldung und abbruch wenn es keine passende Methode zum DIB gibt. Abfragen mit dibMethods.hasOwnProperty(dib);
            const dibMethod = Utils.dibMethods[dib];
            //const dibMethod = Utils.getDIBMethod(dib);
            if(PollumComEStrategy.DEBUG) {console.log('Begin Datenfeld Position steht bei: ',i,'dib : ',dib," mit Länge ",dib.length/2," Methode zum dib ",dibMethod);}


            // Das Byte nach dem DIB auslesen und gucken ob das Extension Bit gesetzt ist(1xxx xxxx --> vib >= 80) //todo verweis auf Protokoll
            //let vib = this.byteAsStringArr[i+dib.length/2]; //todo hier muss nicht +1 sondern + anzahl an Bytes vom dib
            const vib = this.getExtensionBlocks(this.byteAsStringArr, i+dib.length/2);
            if(PollumComEStrategy.DEBUG) {console.log("VIB vor dem Check auf Extenseion: ",vib," als int: ", parseInt(vib,16));}

/*            if(parseInt(vib,16)>=parseInt('0x80',16)) { // parseInt wandelt 0x80 zur Basis 16 (hex) um
                vib += this.byteAsStringArr[i+2]; // Wenn Das Extensionbit gesetzt ist, wird das nächste Byte in Hex als String an das erste Byte gehängt
            }*/
            if(PollumComEStrategy.DEBUG) {console.log("VIB nach dem Check auf Extenseion: ",vib)}
            /*
             vibMethods enthält die Namen der Funktionen zum Verarbeiten der Values mit den VIB (hex) als keys
             Der Rückgabewert sind die Anzahl der Bytes die verarbeitet wurden. Inklusive DIB und VIB
             Dadurch kann dieser wert zum Hochzählen des Schleifencounters zum Iterieren über das Buffer Array genutzt werden

             Der Bracket Operator erlaubt es über den key eine Funktion als Rückgabewert zu bekommen. Der Key ist das VIB (Value Information Byte)

             Als Parameter für die so bekommene Funktion werden immer:
              - die passende Decodierungsfunktion zum DIB (z.B BCD8)
              - der Bytebuffer mit allen Bytes der Nachricht
              - der Zähler, der auf das DIB des aktuellen Wertes Zeigt.
             übergeben
             */
            try {
                i += this.vibMethods[vib](dibMethod, this.byteBuffer, i);
                // falles es den VIB nicht gibt wird die Exception abgefangen.
                // es wird geprüft, ob das extension bit gesetzt wurde oder nicht
                // damm wird die entsprechende Defaultmethode gesetzt und aufgerufen
            }catch (e) {
                console.error(e.message + "Fehler bei versuch mit vib: " + vib);
                let defaultVIB;
                //todo dynamisch auf alle längen checken
                if(parseInt(vib,16)>=parseInt('0x80',16)) {
                     defaultVIB = "ffff"; // Default vib methode für 2 Byte Lange VIBs
                } else {
                     defaultVIB = "00"; // Default vib methode für 1 Byte lange VIBs
                }
                i += this.vibMethods[defaultVIB](dibMethod, this.byteBuffer, i)

            }
        // -3 da das drittletzte Feld eine unbekannte Funktion, es war bisher immer 1f
        // die Letzten beiden Felder sind Prüfsumme und EndeBit
        } while (i < this.byteAsStringArr.length-3)
        this.resetDecoder();

    }

    resetDecoder(){
        // this.byteAsStringArr = []; wird nicht resettet, weil eh bei jedem Byte überschrieben wird.
        this.byteBuffer = Buffer.allocUnsafe(0);
        this.incomingMSGlenght = null;
        this.strategyState = PollumComEStrategy.states.wakeup;
    }
    requestData(msg) {
        console.log('Node: ' + this.parent.guiConfig.name + ' requestData State = ' + this.strategyState)
        this.msg = msg;
        if (msg.payload === PollumComEStrategy.states.request_short || msg.payload === PollumComEStrategy.states.request_long) {
            this.sendRequestMessage()
        } else {
            this.sendSerialMessage(Utils.convertHexStringToByteBuffer(PollumComEStrategy.WAKEUP_HEX_STRING), 'none')
                .then(() => Utils.delay(100)) // Warte 100 ms nach dem ersten Senden
                .then(() => this.sendSerialMessage(Utils.convertHexStringToByteBuffer(PollumComEStrategy.READY_CHECK_HEX_STRING), 'even'))
                .then(()=> this.strategyState = PollumComEStrategy.states.wakeup)
                .catch(error => {
                    console.error('Fehler beim Senden der Nachrichten:', error);
                });
        }
    }

    vibMethods= {
        '06':this.decodeEnergy.bind(this),
        '13':this.decodeVolume.bind(this),
        '2b':this.decodePower.bind(this),
        '3b':this.decodeFlow.bind(this),
        '5a':this.decodeFlowTemp.bind(this),
        '5e':this.decodeReturnFlowTemp.bind(this),
        '60':this.decodeTempDiff.bind(this),
        '6d':this.decodeDateTime.bind(this),
        '78':this.decodeFabricationNumber.bind(this),
        'fd10':this.decodePropertyNumber.bind(this),
        '00':this.defaultOneByteVIB.bind(this),
        'ffff':this.defaultTwoByteVIB.bind(this)

    }
    decodeEnergy(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {
            console.log('Decode Energy VIB value in Wh= ', returnObj.value * 1000);
            //console.log(this);
        }
        this.response.energy = returnObj.value*1000; // Wert in kWh, daher x 1000

        return VIBOffset + returnObj.offset;
    }

    decodeVolume(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Decode Volume VIB value in m³ = ',returnObj.value/1000);}
        this.response.volume =returnObj.value/1000; // Wert in m³ mit 3 Nachkommastellen, daher / 1000
        return VIBOffset + returnObj.offset;
    }
    decodePower(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Decode Power VIB value in W = ',returnObj.value);}
        this.response.power =returnObj.value; // Wert in W
        return VIBOffset + returnObj.offset;
    }
    decodeFlow(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Decode Power VIB value in m³/h = ',returnObj.value);}
        this.response.flow =returnObj.value; // Wert in m³/h
        return VIBOffset + returnObj.offset;
    }
    decodeFlowTemp(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Decode Flow Temp VIB value in °C auf eine Nachkommastelle = ',returnObj.value/10);}
        this.response.flowTemp =returnObj.value/10; // Wert in °C auf eine Nachkommastelle
        return VIBOffset + returnObj.offset;
    }
    decodeReturnFlowTemp(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Decode Return Flow Temp VIB value in °C auf eine Nachkommastelle = ',returnObj.value/10);}
        this.response.returnFlowTemp =returnObj.value/10; // Wert in °C auf eine Nachkommastelle
        return VIBOffset + returnObj.offset;
    }
    decodeTempDiff(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Decode Temperatur Unterschied VIB Wert in °K auf drei Nachkommastelle = ',returnObj.value/1000);}
        this.response.tempDiff =returnObj.value/1000; // Wert in °K auf drei Nachkommastelle
        return VIBOffset + returnObj.offset;
    }
    decodeDateTime(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        const binaryDate = returnObj.value;
        if(PollumComEStrategy.DEBUG) {console.log('Decode Datum = ',binaryDate.toString(2).padStart(32, '0').match(/.{1,8}/g).join(' '));}
        this.response.date = binaryDate & 0x7f;
        return VIBOffset + returnObj.offset;

    }
    decodeFabricationNumber(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Decode Fabrikationsnummer VIB: = ',returnObj.value);}
        this.response.fabNumber =returnObj.value;
        return VIBOffset + returnObj.offset;
    }
    decodePropertyNumber(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 2; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Decode Liegenschaftsnummer  VIB: = ',returnObj.value);}
        this.response.propertyNumber = returnObj.value;
        return VIBOffset + returnObj.offset;
    }
    defaultOneByteVIB(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 1; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Default Decoding = ',returnObj.value);}
        this.response["UnknownVibValue"+this.unkownVIBCounter++] = returnObj.value;
        return VIBOffset + returnObj.offset;
    }
    defaultTwoByteVIB(dibMethod,byteBuffer,iterationStep){
        const VIBOffset = 2; // Anzahl der Bytes die das VIF groß ist
        const returnObj = dibMethod(byteBuffer,iterationStep+VIBOffset);
        if(PollumComEStrategy.DEBUG) {console.log('Default Decoding = ',returnObj.value);}
        this.response["UnknownVibValue"+this.unkownVIBCounter++] = returnObj.value;
        return VIBOffset + returnObj.offset;
    }
    /**
     * Überprüft, ob das Erweiterungsbit für den VIB, DIB oder einen beliebigen Erweiterungsblock gesetzt ist.
     *
     * @param {string} byte - Das Byte, das auf das Erweiterungsbit überprüft werden soll.
     * @return {boolean} True, wenn das Erweiterungsbit gesetzt ist, andernfalls false.
     */
    checkExtensionBit(byte) {
        const extensionBit = parseInt('0x80', 16);
        const byteValue = parseInt(byte, 16);

        return (byteValue & extensionBit) !== 0; // Überprüft per Bitoperation, ob das Erweiterungsbit gesetzt ist
    }

    /**
     * Überprüft, ob das Erweiterungsbit für den VIB, DIB oder einen beliebigen Erweiterungsblock gesetzt ist.
     * Fügt alle Bytes mit gesetztem Erweiterungsbit zur Ergebniszeichenkette hinzu.
     *
     * @param {string[]} byteArray - Das Array von Bytes, das auf das Erweiterungsbit überprüft werden soll.
     * @param {number} startIndex - Der Startindex im Byte-Array.
     * @return {string} Die konkatenierten Bytes mit gesetztem Erweiterungsbit.
     */
    getExtensionBlocks(byteArray, startIndex) {
        let currentIndex = startIndex;
        let extensionBlocks = byteArray[startIndex];
        let extensionCount = 0;

        while (currentIndex < byteArray.length && extensionCount < 9) {
            if (this.checkExtensionBit(byteArray[currentIndex])) {
                extensionBlocks += byteArray[currentIndex+1];
                currentIndex++;
                extensionCount++;
            } else {
                break;
            }
        }

        return extensionBlocks;
    }

}

module.exports = PollumComEStrategy;
