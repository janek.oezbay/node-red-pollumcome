// PrintBytesToConsoleStrategy.js
class PrintBytesToConsoleStrategy {
    interpret(buf) {
        console.log("Empfangen", buf);
    }
}

module.exports = PrintBytesToConsoleStrategy;