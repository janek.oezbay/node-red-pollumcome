// Objekt mit den DIB-Keys und den zugehörigen Methoden
const dibMethods = {
    '02': handleDib02,
    // weitere DIB-Keys und Methoden können hier hinzugefügt werden
};

// Template-Methode zum Interpretieren der DIBs
function interpretDib(dib) {
    const dibKey = dib.toString('hex');
    if (dibMethods.hasOwnProperty(dibKey)) {
        return dibMethods[dibKey](dib);
    }
    return null;
}

// Methoden für die Interpretation der DIBs
function handleDib02(dib) {
    const vif = byteBufferArr[i + 2];
    // Hier weitere Logik für die Interpretation des DIB '02' und des VIF
    // ...

    // Rückgabe des interpretierten Werts
    return interpretedValue;
}

// Funktion zum Analysieren der Antwortnachricht
function parseMbusResponse(buffer) {
    let i = 0;
    let temperatureData = [];

    while (i < buffer.length) {
        const dib = buffer[i];

        // Verarbeite DIB und VIF
        const interpretedValue = interpretDib(dib);
        if (interpretedValue !== null) {
            temperatureData.push(interpretedValue);
            i += calculatedNumberOfBytesForDibVib(dib);
        } else {
            // Unbekanntes DIB, überspringe
            i++;
        }
    }

    return temperatureData;
}
