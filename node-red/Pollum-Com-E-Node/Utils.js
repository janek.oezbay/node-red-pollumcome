// Utils.js
const { Buffer } = require("node:buffer");

class Utils {
    static DEBUG = false;
    //macht aus einem String von HexZahlen einen ByteBuffer
    static convertHexStringToByteBuffer(hexString) {
        // Entferne Leerzeichen aus der Zeichenkette
        hexString = hexString.replace(/ /g, '');

        // Teile die Zeichenkette in Paare von Hex-Werten auf
        const hexArray = hexString.match(/.{1,2}/g);

        // Konvertiere die Hex-Werte in Byte-Werte
        const byteArray = hexArray.map(function (hex) {
            return parseInt(hex, 16);
        });

        // Erzeuge ein Buffer-Objekt aus dem Byte-Array und gib es zurück
        return Buffer.from(byteArray);
    }
    static convertByteBufferToHexArray(byteBuffer) {
        // Konvertiere den Byte-Buffer in ein Array von Hex-Strings
        return Array.from(byteBuffer, byte => byte.toString(16).padStart(2, '0'));
    }
    // Funktion zum Warten einer bestimmten Zeitspanne (in Millisekunden)
    static delay(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    static getDibMethod(dib) {
        const secondChar = dib.charAt(1); // Das zweite Zeichen im String
        switch(secondChar) {
            case '1':
                // Verarbeite Fall, wenn das zweite Zeichen '1' ist
                //8 Bit Integer
                break;
            case '2':
                // Verarbeite Fall, wenn das zweite Zeichen '2' ist
                // 16 Bit Integer
                return this.decodeINT16;
            case '3':
                // Verarbeite Fall, wenn das zweite Zeichen '3' ist
                // 24 Bit Integer
                return this.decodeINT24;
            case '4':
                // Verarbeite Fall, wenn das zweite Zeichen '4' ist
                // 32 Bit Integer
                return this.decodeINT32;
            case '5':
            // Verarbeite Fall, wenn das zweite Zeichen '5' ist
            // 32 Bit Real
            case '6':
            // Verarbeite Fall, wenn das zweite Zeichen '6' ist
            // 48 Bit Integer
            case '7':
            // Verarbeite Fall, wenn das zweite Zeichen '7' ist
            // 64 Bit Integer
            case '8':
                // Verarbeite Fall, wenn das zweite Zeichen '8' ist
                // Selection for Readout
                break;
            case '9':
                // Verarbeite Fall, wenn das zweite Zeichen '9' ist
                // 2 Digit BCD8
                break;
            case 'a':
                // Verarbeite Fall, wenn das zweite Zeichen 'a' ist
                // 4 Digit BCD8
                break;
            case 'b':
                // Verarbeite Fall, wenn das zweite Zeichen 'b' ist
                // 6 Digit BCD8
                break;
            case 'c':
                // Verarbeite Fall, wenn das zweite Zeichen 'c' ist
                // 8 Digit BCD8
                return this.decodeBCD8;
            case 'd':
                // Verarbeite Fall, wenn das zweite Zeichen 'd' ist
                //
                break;
            // Füge weitere Fälle hinzu, falls nötig
            default:
                // Verarbeite den Standardfall, wenn das zweite Zeichen nicht übereinstimmt
                break;
        }
    }
 //todo: Kommentar erklären
    static dibMethods= {
        '02':this.decodeINT16,
        '03':this.decodeINT24,
        '04':this.decodeINT32,
        '0c':this.decodeBCD8,
        '32':this.decodeINT16,
        '8220':this.decode2BytesINT16,
        '8320':this.decode2BytesINT24,
        '8c20':this.decode2BytesBCD8,
        '8c50':this.decode2BytesBCD8,
        '8c60':this.decode2BytesBCD8
    };

    static decodeINT16(byteBuffer,valuePosition,dib) {
        const int16Offset = 1; // Anzahl an Bytes des DIB (Hex02)
        const int16DataLength = 2; // INT16 = 16 Bit = 2 Byte
        const twoByteBuf = byteBuffer.subarray(valuePosition+int16Offset,valuePosition+int16Offset+int16DataLength);
        let value = twoByteBuf.readInt16LE();
        if(Utils.DEBUG) {console.log('Position auf: ',valuePosition, 'twoByteBuf nach subarray= ', twoByteBuf, " readInt16LE: ",value);}
        return {value:value,offset:int16Offset+int16DataLength};
    }
    static decodeINT24(byteBuffer,valuePosition,dib) {
        const int24Offset = 1; // Anzahl an Bytes des DIB (Hex 03)
        const int24DataLength = 3; // 24 Bit = 3 Bytes

        const threeByteBuf = byteBuffer.subarray(valuePosition,valuePosition+int24DataLength);

        // Javascript biete keine readInt24LE Funktion an. Daher werden noch 8 Bit an den 24 Bit Buffer angehängt. Da es Little Endian Codiert ist wird es wie voranstehende Dezimal nullen gewertet
        const zeroBuffer = Buffer.from("00",'hex');
        const fourByteBuf = Buffer.concat([threeByteBuf,zeroBuffer],4);

        if(Utils.DEBUG) {console.log('fourByteBuf nach subarray und concat= ', fourByteBuf);}
        const value = fourByteBuf.readInt32LE();

        return {value:value,offset:int24Offset+int24DataLength};
    }

    static decodeINT32(byteBuffer,valuePosition,dib) {
        const int32Offset = 1; // Anzahl an Bytes des DIB (Hex 04)
        const int32DataLength = 4; // 32 Bit = 4 Bytes
        const fourByteBuf = byteBuffer.subarray(valuePosition,valuePosition+int32DataLength);
        const value = fourByteBuf.readUInt32LE();
        return {value:value,offset:int32Offset+int32DataLength};
    }
    // BCD benötigt 4 Bit pro Dezimalzahl. Da 8 Dezimalstellen Codiert werden
    static decodeBCD8(byteBuffer,valuePosition,dib) {
        const BCD8Offset = 1; // Anzahl der Bytes die das BCD8 encodeieren (0c).
        const bcd8DataLength = 4; // BCD8 wird in 8 Nibbles = 4 Bytes gespeichert
        const byte1 = byteBuffer.toString('hex',valuePosition+BCD8Offset+3,valuePosition+BCD8Offset+4);
        const byte2 = byteBuffer.toString('hex',valuePosition+BCD8Offset+2,valuePosition+BCD8Offset+3);
        const byte3 = byteBuffer.toString('hex',valuePosition+BCD8Offset+1,valuePosition+BCD8Offset+2);
        const byte4 = byteBuffer.toString('hex',valuePosition+BCD8Offset+0,valuePosition+BCD8Offset+1);

        const value = parseInt(byte1+byte2+byte3+byte4);
        if(Utils.DEBUG) {console.log("Byte1: ", byte1,"Byte2: ", byte2,"Byte3: ", byte3,"Byte4: ", byte4, " Ergibt :",value);}

        return {value:value,offset:BCD8Offset+bcd8DataLength};// +4 weil 4 Daten Bytes
    }

    static decode2BytesINT16(byteBuffer,valuePosition,dib) {
        const int16Offset = 2; // Anzahl an Bytes des DIB (Hex02)
        const int16DataLength = 2; // INT16 = 16 Bit = 2 Byte
        const twoByteBuf = byteBuffer.subarray(valuePosition+int16Offset,valuePosition+int16Offset+int16DataLength);
        let value = twoByteBuf.readInt16LE();
        if(Utils.DEBUG) {console.log('Position auf: ',valuePosition, 'twoByteBuf nach subarray= ', twoByteBuf, " readInt16LE: ",value);}
        return {value:value,offset:int16Offset+int16DataLength};
    }

    static decode2BytesINT24(byteBuffer,valuePosition,dib) {
        const int24Offset = 2; // Anzahl an Bytes des DIB (Hex 03)
        const int24DataLength = 3; // 24 Bit = 3 Bytes

        const threeByteBuf = byteBuffer.subarray(valuePosition,valuePosition+int24DataLength);

        // Javascript biete keine readInt24LE Funktion an. Daher werden noch 8 Bit an den 24 Bit Buffer angehängt. Da es Little Endian Codiert ist wird es wie voranstehende Dezimal nullen gewertet
        const zeroBuffer = Buffer.from("00",'hex');
        const fourByteBuf = Buffer.concat([threeByteBuf,zeroBuffer],4);

        if(Utils.DEBUG) {console.log('fourByteBuf nach subarray und concat= ', fourByteBuf);}
        const value = fourByteBuf.readInt32LE();

        return {value:value,offset:int24Offset+int24DataLength};
    }
    static decode2BytesBCD8(byteBuffer,valuePosition) {
        const BCD8Offset = 2; // Anzahl der Bytes die das BCD8 encodeieren (0c).
        const bcd8DataLength = 4; // BCD8 wird in 8 Nibbles = 4 Bytes gespeichert
        const byte1 = byteBuffer.toString('hex',valuePosition+BCD8Offset+3,valuePosition+BCD8Offset+4);
        const byte2 = byteBuffer.toString('hex',valuePosition+BCD8Offset+2,valuePosition+BCD8Offset+3);
        const byte3 = byteBuffer.toString('hex',valuePosition+BCD8Offset+1,valuePosition+BCD8Offset+2);
        const byte4 = byteBuffer.toString('hex',valuePosition+BCD8Offset+0,valuePosition+BCD8Offset+1);

        const value = parseInt(byte1+byte2+byte3+byte4);
        if(Utils.DEBUG) {console.log("Byte1: ", byte1,"Byte2: ", byte2,"Byte3: ", byte3,"Byte4: ", byte4, " Ergibt :",value);}

        return {value:value,offset:BCD8Offset+bcd8DataLength};// +4 weil 4 Daten Bytes
    }

    // Hier können weitere Util-Funktionen hinzugefügt werden, wenn benötigt
}

module.exports = Utils;
